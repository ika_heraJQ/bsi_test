import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:sbi_test/controllers/generatenumbercontroller.dart';

void main() => {
  runApp(MyApp())
};

class MyApp extends StatelessWidget {
  GenerateNumberController generateNumberController = Get.put(GenerateNumberController());

  Widget input(BuildContext context){
    return TextFormField(
      controller: generateNumberController.inputVal,
      decoration: InputDecoration(
        hintText: "Input N", 
        contentPadding: const EdgeInsets.all(15.0),
        border: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.black, width: 3, style: BorderStyle.solid),
          borderRadius: BorderRadius.circular(0),
          gapPadding: 3
        ),
        suffixIcon: IconButton(
          onPressed: () => generateNumberController.inputVal.clear(), 
          icon: const Icon(Icons.clear)
        )
      ),
      validator: (text) {
        if(text!.isEmpty){
          return 'Value can\'t be empty';
        }
        return null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: Scaffold(
        body: Container(
            padding: const EdgeInsets.only(top : 100, left: 20, right: 20),
            child: Column(
              //padding: const EdgeInsets.all(20),
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: generateNumberController.inputVal,
                  decoration: InputDecoration(
                    hintText: "Input N", 
                    contentPadding: const EdgeInsets.all(15.0),
                    border: OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.black, width: 3, style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(0),
                      gapPadding: 3
                    ),
                    suffixIcon: IconButton(
                      onPressed: () => generateNumberController.inputVal.clear(), 
                      icon: const Icon(Icons.clear)
                    )
                  ),
                  validator: (text) {
                    if(text!.isEmpty){
                      return 'Value can\'t be empty';
                    }
                    return null;
                  },
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    ElevatedButton(
                      onPressed: (){
                        generateNumberController.generateNumberButton1();
                      }, 
                      child: const Text('1', textAlign: TextAlign.center)
                    ),
                    ElevatedButton(
                      onPressed: (){
                        generateNumberController.generateNumberButton2();
                      }, 
                      child: const Text('2', textAlign: TextAlign.center)
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    ElevatedButton(
                      onPressed: (){
                        generateNumberController.generateNumberButton3();
                      }, 
                      child: const Text('3', textAlign: TextAlign.center)
                    ),
                    ElevatedButton(
                      onPressed: (){
                        generateNumberController.generateNumberButton4();
                      }, 
                      child: const Text('4', textAlign: TextAlign.center)
                    )
                  ],
                ),
                const SizedBox(height: 50),
                const Text('Results:', textAlign: TextAlign.left, style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                const SizedBox(height: 20),
                GetBuilder<GenerateNumberController>(
                  builder: (controller) {
                    return Expanded(
                      child: SingleChildScrollView(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Text(
                          generateNumberController.generatedValue,
                          style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold)
                        )
                      )
                    );
                  }
                )
              ],
            ),
          )
      )
    );
  }
}