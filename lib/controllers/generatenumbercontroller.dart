import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GenerateNumberController extends GetxController {
  TextEditingController inputVal = TextEditingController();
  var generatedValue = '';

  void generateNumberButton1() {
    int? number = int.tryParse(inputVal.text);
  
    var listNum = List<int>.generate(number!, (index) => index + 1);
    generatedValue = listNum.join(' ');
    update();
  }

  void generateNumberButton2() {
    int? number = int.tryParse(inputVal.text);

    var listNum = List<int>.generate(number!, (index) => index + 1);
    var listNum2 = List<int>.generate((number-1), (index) => index + 1);
    listNum = listNum + listNum2.reversed.toList();
    generatedValue = listNum.join(' ');
    update();
  }

  void generateNumberButton3() {

  }

  void generateNumberButton4() {

  }

}